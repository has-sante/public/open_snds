# -*- coding: utf-8 -*-
# %%
import os

os.chdir("..")
# %%
import re
import pandas as pd

pd.set_option("display.max_columns", None)
# %% # Documentations
xl_opendamir = pd.ExcelFile("documentation/Lexique_open_damir.xls")
open_damir_vars = {}
for sheet in xl_opendamir.sheet_names[2:]:
    open_damir_vars[sheet] = pd.read_excel(xl_opendamir, sheet)
# TODO recupérer les données référentiels depuis schema snds

# %% # Other referentiels
# %%
def transform_year_sex(df):
    cl_cols = df.columns[2:-1]
    cl_cols = [re.search("(\d\d*) à (\d\d*)", col) for col in cl_cols]
    cl_cols = [col.group(1) + "_" + col.group(2) for col in cl_cols]
    cl_cols = ["dep_ix", "dep_lib"] + cl_cols + ["95_120"]
    df.columns = cl_cols
    df_cl = df.set_index(["dep_ix", "dep_lib"]).stack()
    df_cl = df_cl.reset_index()
    # df_cl.columns[:2] = ['age_classe', 'count']
    return df_cl.rename(columns={"level_2": "age_class", 0: "count"})


xl_pop = pd.ExcelFile("referentiels/raw/estim-pop-dep-sexe-aq-1975-2020.xls")
pop_by_year = {}
for sheet in xl_pop.sheet_names[1:]:
    pop_by_year[sheet] = pd.read_excel(xl_pop, sheet, skiprows=4, skipfooter=4)

for year, pop in pop_by_year.items():
    pop_cl = pop.loc[pop.loc[:, "Unnamed: 1"] != "France métropolitaine"]
    pop_cl_m = pd.concat((pop_cl.iloc[:, :2], pop_cl.iloc[:, 23:43]), axis=1)
    pop_cl_f = pd.concat((pop_cl.iloc[:, :2], pop_cl.iloc[:, 44:64]), axis=1)
    pop_cl_m = transform_year_sex(pop_cl_m)
    pop_cl_f = transform_year_sex(pop_cl_f)
    pop_cl_m["sex"] = "M"
    pop_cl_f["sex"] = "F"
    pop_cl = pd.concat((pop_cl_f, pop_cl_m), axis=0)
    pop_cl["year"] = int(year)
    pop_by_year[year] = pop_cl[
        ["year", "dep_ix", "dep_lib", "sex", "age_class", "count"]
    ]

all_populations = pd.concat(pop_by_year.values(), axis=0)
all_populations.to_csv("referentiels/population_sexe_dep_year.csv")
# %%
