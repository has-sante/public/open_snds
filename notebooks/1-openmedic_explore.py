# %%
from libpath import Path
import logging
from IPython import get_ipython
import pandas as pd
from opensnds.constants import *
from opensnds.config import SUPPORTED_DBS
import os
from opensnds.preprocessing.schema_handler import load_schemas, load_schema
from opensnds.app.utils.back_utils import load_datasets

logging.basicConfig(level=logging.INFO)
get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")
# %%
dbname = "openbio"
schema = load_schema(dbname)
db = pd.read_parquet(DIR2INTERIM_OPENBIO)
# openmedic.createOrReplaceTempView("openmedic")
# openmedic_df = openmedic.limit(1000).toPandas()
print(db.shape)
# %%
most_expensive = db.groupby("L_ACTE").agg(
    **{
        "rbt": pd.NamedAgg("REM", "sum"),
        "bse": pd.NamedAgg("BSE", "sum"),
        "dnb": pd.NamedAgg("DNB", "sum"),
    }
)


most_expensive["prix_unite"] = most_expensive["bse"] / most_expensive["dnb"]
most_expensive.query("dnb >= 10000").sort_values("prix_unite", ascending=False).head(15)

# %%
most_expensive.sort_values("dnb").tail()

# %%
db.query("L_ACTE == 'POTASSIUM.'")

# %%
pd.read_csv(Path(DIR2RAW_OPENBIO, ""))

# %%
