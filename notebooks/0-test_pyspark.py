# %%
from pyspark.sql import SparkSession
import pandas as pd
import numpy as np

# %%
# mock data
dims = (1000, 1000)
colnames = [f"col_{i}" for i in range(dims[1])]
test_df = pd.DataFrame(np.random.rand(*dims), columns=colnames)
test_df.head()
# %%
# pyspark session
sc = SparkSession.builder.appName("opensnds").getOrCreate()

# %%
spark_df = sc.createDataFrame(test_df)
print(spark_df.count())
spark_df.select(["col_0", "col_1"]).show(5)
# %%
