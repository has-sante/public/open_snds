# -*- coding: utf-8 -*-
# %%
from pyspark.sql.functions import regexp_replace
from pyspark.sql import DataFrame as psdf
import logging
from IPython import get_ipython
from opensnds.utils import sc
import pandas as pd
from opensnds.constants import *
from opensnds.config import SUPPORTED_DBS
import os
from opensnds.preprocessing.schema_handler import load_schema, join_nomenclatures

logging.basicConfig(level=logging.INFO)
get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")
# %%
# Renseigner la base à explorer
dbname = "openlpp"
schema = load_schema(dbname)
db = pd.read_parquet(DIR2CLEAN_OPENLPP)
print(db.shape)
# %%
db.head()

# %%
db.max()

# %%
