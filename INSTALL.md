# Utilisation rapide

Nous utilisons podman et podman-compose pour conteneuriser l'application. Après avoir installé podman et podman-compose :

- Télécharger les données en suivant les urls données dans le `README.md` et peupler les dossiers `data/raw` pour chaque dataset :  `data/raw/{dataset_name}` (openbio, opendamir, openmedic, openlpp).
- Lancer `make compile`. Ce script compile les fichiers bruts csv en les rassemblant en un seul fichier par base et effectue le typage des données. Il est censé être rapide (quelques minutes) sauf pour la base opendamir (plusieurs heures).
- Copier et adapter le fichier d'environnement : `cp template.env .env`
- Lancer l'application avec `make up-prod` : Consulter l'url pour 127.0.0.1:7777

Pour apporter des modifications, on peut lancer un conteneur podman de l'application sur le port 8050 en lançant `make run-dev-container` puis `poetry run ./app`.

# Architecture du code

## Téléchargement et compilation des datasets

Dans le dossier `opencncam/data`, se trouvent les différentes étapes *d'acquisition* des données.

**Pré-requis :** Téléchargements des fichiers sources sur les liens de la CNAM. L'automatisation de cette partie est compliquée à cause d'un captcha.

Utilisation par défault :

```
python opensnds/data/compile_data.py
```
Pour chacune des bases, ce script :

- télécharge les schémas et les nomenclatures liée à une base sur le gitlab du schema-SNDS si ceux-ci sont disponibles
- Pour chaque base de donnée demandée en entrée (par défaut: openmedic, openbio, openlpp, opendamir), lit et empile chaque fichier (mode **append**) dans une base unique en format compressé et orienté colonnes parquet.
- Pour les bases contenant des montants, normalise en double les colonnes de montant

## Preprocessing des données

Les scripts concernés sont dans le dossier `preprocessing`.
Ils ont servi initialement à créer les schémas qui n'existaient pas pour les bases openmedic, openbio et opendamir. Cependant, ces schémas sont désormais disponibles sur le git du [schéma SNDS](https://gitlab.com/healthdatahub/schema-snds).  

Cette étape :
- créée les schémas et les nomenclatures qui n'existaient pas sur le site du schema-SNDS.
- Pour les petites bases de données (openbio et openlpp), sépare les colonnes de nomenclatures parfois présente dans la base source.

*NOTE*: Les schémas et nomenclatures supplémentaires qui ont déjà été créés ne seront pas écrasés.

## podman

Nous utilisons podman pour gérer les environnements de processing des données ainsi que ceux de développement et de production de l'application.

A chaque étape est associée un docker-compose.yml :
 - docker-compose.compile.yml : pour lancer le preprocessing des données brutes
 - docker-compose.yml : pour lancer l'application en production (port 7777)

Pour lancer une étape <etape>.yml, lancer la commande suivante : `podman-compose -f <etape>.yml up -d`

Pour voir les logs associés à une étape, lancer : `podman-compse -f <etape>.yml logs -f`

Ces commandes sont résumées dans un makefile avec des alias pour accéler les tests : `make <alias>`.
