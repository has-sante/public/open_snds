import io
import logging
import os
import re
import json
import requests
import zipfile
from datetime import datetime
from opensnds.constants import (
    DIR2INTERIM,
    DIR2NOMENCLATURES,
    DIR2RAW,
    DIR2SCHEMAS,
    GITLAB_COM_API_V4,
    SCHEMA_SNDS_GITLAB_ID,
    SCHEMA_SNDS_GITLAB_RAW_URL,
    SUPPORTED_EXTENSIONS,
    STORAGE_OPTIONS,
    FS,
)
from opensnds.config import SUPPORTED_DBS
from typing import List
from pathlib import Path
import pandas as pd


logger = logging.getLogger(__name__)

# TODO: on peut stream du gz


def read_single_panda_dataframe(path2file: Path):
    """Panda reader for a single file

    Args:
        path2file (Path): path to a csvn or a csv.zip

    Returns:
        psdf: inmemory lazy evaluated spark dataframe
    """
    logger.warning(f"Read file {path2file.stem}")
    path2csv = path2file.with_suffix(".csv")
    # zipped csv files are hard to read directly with spark, so unzip them
    if path2file.match("*.zip") and not path2csv.is_file():
        with zipfile.ZipFile(path2file, "r") as zip_ref:
            zip_ref.extractall(path2csv)
        # remove the original zip to avid duplicates
        os.remove(path2file)
        path2file = path2csv
    df = pd.read_csv(path2file, delimiter=";", encoding="latin-1", engine="python")
    df.columns = [x.upper() for x in df.columns]
    return df


def process_dataset(db_name: str, clean_db=False):
    config = SUPPORTED_DBS[db_name]
    if config["consommants"]:
        source_dirs = [
            str(DIR2RAW / Path(db_name) / Path(lvl.lower()))
            for lvl in config["level_choices"]
        ]
        target_paths = [
            str(DIR2INTERIM / Path(db_name) / Path(lvl.lower()))
            for lvl in config["level_choices"]
        ]
    else:
        source_dirs = [str(DIR2RAW / Path(db_name))]
        target_paths = [str(DIR2INTERIM / Path(db_name))]
    for source_dir, target_path in zip(source_dirs, target_paths):
        process_sub_dataset(db_name, source_dir, target_path, clean_db=clean_db)


def process_sub_dataset(
    db_name: str,
    source_dir: str,
    target_path: str,
    clean_db: bool = False,
    subset: List[str] = None,
):
    """Read all csvs in `source_dir`applying `dbname` config with spark and write them in append mode to a single file at `target_path`

    Args:
        db_name (str): The name of the database, corresponds to the name of the subdirectory in raw or interim data folders.
        clean_db (bool, optional): Clean the original DB if true. Defaults to False.
        subset (List[str], optional): subset of file to append (eg. in update mode). Defaults to None.
    """
    logger.warning("Début de consolidation de la base {}".format(source_dir))
    t0 = datetime.now()
    # # TODO : Rajouter la suppression dans S3
    if clean_db and FS.isdir(target_path):
        FS.rm(target_path, recursive=True)
        logger.warning(f"Removing directory at {target_path}")
    if not FS.isdir(target_path):
        FS.makedirs(target_path, exist_ok=True)
    config = SUPPORTED_DBS[db_name]

    frequency = config["frequency"]
    dataset_dir = Path(source_dir)
    files_to_process = sorted(FS.ls(dataset_dir))
    if subset is not None:
        files_to_process = list(set(files_to_process).intersection(subset))
    for filename in files_to_process:
        path2file = Path(filename)
        if re.search("|".join(SUPPORTED_EXTENSIONS), filename):
            df_name = re.sub("|".join(SUPPORTED_EXTENSIONS), "", filename).lower()
            df = read_single_panda_dataframe(path2file)
            # Add the frequency column (eg. year)
            if frequency is not None:
                df_frequency = re.search(frequency[1], df_name)
                df["year"] = int(df_frequency.group(0))
                df.info()
            # specific cleaning
            # processing specifique
            if db_name in ["openbio", "openmedic"]:
                df = clean_montants(df)
            # opebio columns for acts change as of 2017
            if db_name == "openbio":
                if "BIO_PRS_IDE" in df.columns:
                    df.rename(
                        columns={"BIO_PRS_IDE": "ACTE", "BIO_INF_IDE": "L_ACTE"},
                        inplace=True,
                    )
            # TODO: enforce the schema of the DB loaded from schema-snds
            # schema = get_schema(db_name)
            # df = enforce_schema(df, schema)
            # write df to parquet
            df.to_parquet(
                f"{target_path}/",
                engine="pyarrow",
                compression="snappy",
                index=None,
                partition_cols=["year"],
                storage_options=STORAGE_OPTIONS,
            )
            logger.info(
                "Sauvegarde au format parquet de la table {} dans {}".format(
                    df_name, target_path
                )
            )
        else:
            logger.error(f"Extension is not supported, got: {filename}")
    logger.info("Fin de consolidation pour la base {}\n".format(source_dir))
    logger.info("Consolidation en {}".format(datetime.now() - t0))


def clean_montants(df):
    """Les montants sont indiqués avec des séparateurs en . pour les milliers et , pour les décimales, il faut nettoyer cela avant de caster dans un type double.

    Args:
        df (psdf): [description]

    Returns:
        [type]: [description]
    """
    df = df.withColumn("REM", regexp_replace("REM", "\.", ""))
    df = df.withColumn("REM", regexp_replace("REM", ",", ".").cast("double"))
    df = df.withColumn("BSE", regexp_replace("BSE", "\.", ""))
    df = df.withColumn("BSE", regexp_replace("BSE", ",", ".").cast("double"))
    return df


def clean_montants(df: pd):
    print("Process Panda Dataframe -> clean montants")
    df["REM"] = df["REM"].str.replace(".", "")
    df["REM"] = df["REM"].str.replace(",", ".").astype(float)
    df["BSE"] = df["BSE"].str.replace(".", "")
    df["BSE"] = df["BSE"].str.replace(",", ".").astype(float)
    return df


# Fonctions utilitaires pour télécharger les schémas et nomenclatures


def download_schemas(verify=False):
    for db_name, db_config in SUPPORTED_DBS.items():
        url_schema = db_config["url_schema"]
        try:
            r = requests.get(url_schema, verify=verify)
            json.dump(r.json(), open(Path(DIR2SCHEMAS, db_name + ".json"), "w"))
        except:
            print(f"No schema found at url: {url_schema}")


def download_nomenclatures(verify=False):
    """
    Download nomenclatures from the [schema snds repository](https://gitlab.com/healthdatahub/schema-snds/-/blob/master/src/byproducts/update_byproducts_repositories.py)
    """
    for dbname, db_config in SUPPORTED_DBS.items():
        if "nomenclatures_dir_path" in db_config.keys():
            logger.info(f"Downloading nomenclatures for {dbname}")
            os.makedirs(os.path.join(DIR2NOMENCLATURES), exist_ok=True)
            nomenclatures_dir_path = db_config["nomenclatures_dir_path"]
            # first call to list available files for this database
            nomenclatures_dir_url = f"{GITLAB_COM_API_V4}/projects/{SCHEMA_SNDS_GITLAB_ID}/repository/tree?path={nomenclatures_dir_path}"
            # neeed to specify number of returned elements for gitlab api
            payload = {"per_page": 500}
            r = requests.get(nomenclatures_dir_url, verify=verify, params=payload)
            list_of_files = r.json()
            nomenclature_names = set(
                [
                    re.sub("\..*$", "", description["name"])
                    for description in list_of_files
                ]
            )
            nomenclatures_url = (
                f"{SCHEMA_SNDS_GITLAB_RAW_URL}/nomenclatures/{dbname.upper()}/"
            )
            for nomenclature_name in nomenclature_names:
                download_nomenclature_by_name(
                    nomenclature_name, nomenclatures_url, verify
                )


def download_nomenclature_by_name(nomenclature_name, nomenclatures_url, verify=False):
    """Download a specific nomenclature given a public gitlab url to the nomenclature and a nomenclature name.

    Args:
        nomenclature_name ([type]): [description]
        nomenclature_url ([type]): [description]
        verify (bool, optional): [description]. Defaults to False.
    """
    logger.debug(f"Downloading at {nomenclatures_url}")
    success = 0
    os.makedirs(DIR2NOMENCLATURES, exist_ok=True)
    try:
        path2localsave = Path(DIR2NOMENCLATURES, nomenclature_name + ".json")
        file_url = nomenclatures_url + f"{nomenclature_name}.json"
        r = requests.get(file_url, verify=verify)
        json.dump(r.json(), open(path2localsave, "w"))
        success += 1
    except:
        logger.warning("No json found at {}".format(file_url))
    try:
        path2localsave = Path(DIR2NOMENCLATURES, nomenclature_name + ".csv")
        file_url = nomenclatures_url + f"{nomenclature_name}.csv"
        r = requests.get(file_url, verify=verify)
        try:
            df = pd.read_csv(io.StringIO(r.content.decode()), sep=";")
        except:
            df = pd.read_csv(io.StringIO(r.content.decode()), sep=",")
        df.to_csv(path2localsave, index=False)
        success += 1
    except:
        logger.warning("No csv found at {}".format(file_url))
    if success >= 2:
        logger.info(f"Found nomenclature csv and json at {nomenclatures_url}")
    return success == 2
