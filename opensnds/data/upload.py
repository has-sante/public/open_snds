import fsspec

from opensnds.constants import (
    DIR2NOMENCLATURES,
    DIR2RAW,
    DIR2SCHEMAS,
    PATH2STORAGE,
    STORAGE_OPTIONS,
)

fs = fsspec.filesystem("s3", **STORAGE_OPTIONS)


def upload_raw_data():
    fs.put(str(DIR2RAW), f"{PATH2STORAGE}/data/raw/", recursive=True)
    fs.put(
        str(DIR2NOMENCLATURES), f"{PATH2STORAGE}/data/nomenclatures/", recursive=True
    )
    fs.put(str(DIR2SCHEMAS), f"{PATH2STORAGE}/data/schemas/", recursive=True)
