import requests
import time
import grequests

headers = {"content-type": "application/json"}
default_request = '{"output":"..fig-sex.figure...fig-age.figure...fig-reg.figure...fig-time.figure...output-sex-df.children...output-age-df.children...output-reg-df.children...output-time-df.children...dashboard-level-choices.options...dashboard-filter-values.options...dashboard-year-choices.options...dashboard-level-choices.value...dashboard-filter-values.value..","outputs":[{"id":"fig-sex","property":"figure"},{"id":"fig-age","property":"figure"},{"id":"fig-reg","property":"figure"},{"id":"fig-time","property":"figure"},{"id":"output-sex-df","property":"children"},{"id":"output-age-df","property":"children"},{"id":"output-reg-df","property":"children"},{"id":"output-time-df","property":"children"},{"id":"dashboard-level-choices","property":"options"},{"id":"dashboard-filter-values","property":"options"},{"id":"dashboard-year-choices","property":"options"},{"id":"dashboard-level-choices","property":"value"},{"id":"dashboard-filter-values","property":"value"}],"inputs":[{"id":"input-dataset-name","property":"value","value":"openlpp"},{"id":"dashboard-level-choices","property":"value","value":"TITRE"},{"id":"dashboard-query-button","property":"n_clicks","value":1}],"changedPropIds":["dashboard-query-button.n_clicks"],"state":[{"id":"dashboard-filter-values","property":"value","value":null},{"id":"dashboard-input-list-codes","property":"value"},{"id":"dashboard-year-choices","property":"value","value":2019}]}'
filter_request = '{"output":"dashboard-clipboard-sql-age.children","outputs":{"id":"dashboard-clipboard-sql-age","property":"children"},"inputs":[{"id":"click-query-sql-age","property":"n_clicks"},{"id":"input-dataset-name","property":"value","value":"openmedic"},{"id":"dashboard-level-choices","property":"value","value":"ATC5"},{"id":"dashboard-filter-values","property":"value","value":["P02CF01","P01BA02","J01FA10"]},{"id":"dashboard-year-choices","property":"value","value":2019}],"changedPropIds":["dashboard-level-choices.value","dashboard-filter-values.value"]}'


def curl():
    url = "http://127.0.0.1:8050/_dash-update-component"
    r = requests.post(url, data=default_request, headers=headers)
    print(r.text)


def bench(n=10, payload=[default_request, filter_request]):
    start = time.time()
    url = "http://127.0.0.1:8050/_dash-update-component"
    rs = (grequests.post(url, data=payload[1], headers=headers) for i in range(0, n))
    result = grequests.map(rs)
    print(time.time() - start)
    print(result)
