#### Open Bio: base complète sur les dépenses de biologie médicale interrégimes

L’offre de données Open Bio est constituée d’un ensemble de bases annuelles, portant sur la consommation d’actes de biologie médicale en 2014 à 2019. Elle fournit des informations complémentaires au fichier BiolAM.

Les données disponibles concernent les montants remboursés en ville et le nombre de bénéficiaires.
Les données sont ventilées par année, classe d'âge, sexe, et région. Il s'agit de données agrégées, il n'est donc pas possible de croiser deux variables pour obtenir le nombre de bénéficiaires exactes.