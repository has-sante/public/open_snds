#### Open Medic : base complète sur les dépenses de médicaments interrégimes

L’offre de données Open Medic est constituée d’un ensemble de bases annuelles, portant sur l’usage du médicament, délivré en pharmacie de ville de 2014 à 2019. Elle fournit des informations complémentaires au fichier [Medic’AM]

Les données disponibles concernent les montants remboursés en ville et le nombre de bénéficiaires.
Les données sont ventilées par année, classe d'âge, sexe, et région. Il s'agit de données agrégées, il n'est donc pas possible de croiser deux variables pour obtenir le nombre de bénéficiaires exactes.

#### Liens
 - [Sur datagouv.fr](https://www.data.gouv.fr/fr/datasets/open-medic-base-complete-sur-les-depenses-de-medicaments-interregimes/)
 - [Sur ameli](http://open-data-assurance-maladie.ameli.fr/medicaments/)
