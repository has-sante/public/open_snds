#### Open LPP : base complète sur les dépenses de dispositifs médicaux inscrits à la liste de produits et prestations (LPP) interrégimes OPENLPP

L’offre de données Open LPP est constituée d’un ensemble de bases annuelles, portant sur les remboursements et le nombre de bénéficiaires de dispositifs médicaux inscrits à la liste de produits et prestations de 2014 à 2018. Elle fournit des informations complémentaires au fichier LppAM.

Les données disponibles concernent les montants remboursés en ville et le nombre de bénéficiaires.
Les données sont ventilées par année, classe d'âge, sexe, et région. Il s'agit de données agrégées, il n'est donc pas possible de croiser deux variables pour obtenir le nombre de bénéficiaires exactes.

#### Liens
 - [Sur datagouv.fr](https://www.data.gouv.fr/fr/datasets/open-lpp-base-complete-sur-les-depenses-de-dispositifs-medicaux-inscrits-a-la-liste-de-produits-et-prestations-lpp-interregimes/)
 - [Sur ameli](http://open-data-assurance-maladie.ameli.fr/LPP/index.php)