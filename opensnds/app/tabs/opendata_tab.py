import dash_bootstrap_components as dbc
import dash_html_components as html
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
from opensnds.constants import DIR2ASSETS
from opensnds.app.constants import home_page_md, encoded_image, RAPPORT_PAGENAME
import os

layout = html.Div(
    children=[
        html.P(
            [
                "Cette application expose les données ouvertes du ",
                html.A(
                    "portail open data de l'Assurance Maladie",
                    href="https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/bases-de-donnees-open-data/liste-bases-de-donnees-open-data",
                ),
                ". La HAS n'est pas responsable de la qualité de ces données ou des possibles écarts avec les données individuelles du SNDS.",
            ]
        ),
        html.H4("Jeux de données disponibles dans l'application"),
        html.P(
            """
        L'application expose les données sur les remboursement en ville :
        """
        ),
        html.Ul(
            [
                html.Li("des médicaments (Open Medic)"),
                html.Li("des actes de biologie (Open Bio)"),
            ]
        ),
        html.H5("Origine et nature des données"),
        html.P(
            """
                Ces données sont des statistiques, extraites du système national des données de santé (SNDS).
                Elles sont publiées annuellement par la Caisse nationale de l'assurance maladie depuis 2014.
                Les statistiques disponibles concernent les montants remboursés en ville, le nombre de bénéficiaires et le nombre de prestations, effectuées par l'ensemble des régimes d'assurance maladie.
                Ces statistiques sont ventilées par : année; classe d'âge, sexe, et région du bénéficiaire; spécialité du prescripteur."""
        ),
        html.H5(
            "Open Medic : base complète sur les dépenses de médicaments interrégimes"
        ),
        html.P(
            [
                "L’offre de données Open Medic porte sur l’usage du médicament, délivré en pharmacie de ville sur le territoire français (métropole et outre-mer).",
                "Elle fournit des informations complémentaires au fichier Medic’AM.",
                html.Br(),
                "Ressources : ",
                html.A(
                    "Ameli",
                    href="https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/bases-de-donnees-open-data/liste-bases-de-donnees-open-data#text_133953",
                ),
                ", ",
                html.A(
                    "datagouv.fr",
                    href="https://www.data.gouv.fr/fr/datasets/open-medic-base-complete-sur-les-depenses-de-medicaments-interregimes/",
                ),
            ]
        ),
        html.H5(
            "Open Bio : base complète sur les dépenses de biologie médicale interrégimes"
        ),
        html.P(
            [
                "L’offre de données Open Bio porte sur la consommation d’actes de biologie médicale sur le territoire français (métropole et outre-mer). ",
                "Elle fournit des informations complémentaires au fichier BiolAM.",
                html.Br(),
                "Ressources : ",
                html.A(
                    "Ameli",
                    href="https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/bases-de-donnees-open-data/liste-bases-de-donnees-open-data#text_133965",
                ),
                ", ",
                html.A(
                    "datagouv.fr",
                    href="https://www.data.gouv.fr/fr/datasets/open-bio-base-complete-sur-les-depenses-de-biologie-medicale-interregimes/",
                ),
            ]
        ),
        html.H4("Limites et mises en gardes sur l'usage des données"),
        html.H5("Aggrégation"),
        html.P(
            """Les données exposées sont des données statistiques, agrégées et figées.
Si l'on sélectionne sur plusieurs codes simultanément, le nombre de bénéficiaires affiché est la somme du nombre de bénéficiaires pour chacun des codes, ce qui est une borne haute du nombre de bénéficiaires réel. En effet, si un même bénéficiaire a consommé des prestations pour 2 codes, il sera comptabilisé 2 fois.
"""
        ),
        html.P(
            """
        ⚠ L'écart de la borne haute à la réalité est particulièrement important lorsque l'on sélectionne des codes concernant de nombreux bénéficiaires, notamment aux premiers niveaux des hiérarchies de terminologies.
Les analyses combinant plusieurs codes nécessiteront donc de retourner aux données SNDS individuelles sur le portail de l'assurance maladie.
        """
        ),
        html.H5("Secret statistique"),
        html.P(
            """
            Ces données statistiques sont anonymisées lors de leur publication afin de garantir la confidentialité des informations sur les bénéficiaires ainsi que sur les professionnels de santé.
            La méthode employée est une k-anonymisation, qui consiste à masquer les informations lorsque le nombre de bénéficiaires concernés est supérieur à 1O.
            En pratique, cette opération est réalisée en retirant certaines variables de croisement, par exemple en retirant la distinction par sexe ou par région.
            Ceci explique la présence de "valeurs inconnues" dans l'application.

            Note : Il est possible que des lignes entières soient supprimées pour des prestations réalisées chez moins de 10 personnes sur une année.
            """
        ),
    ],
    style={"width": "80%"},
)
