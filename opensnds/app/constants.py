import base64
import seaborn as sns
from opensnds.constants import (
    DIR2ASSETS,
    DIR2DATA_INFORMATION,
    DIR2INTERIM,
    FS,
)
from opensnds.config import SUPPORTED_DBS
import plotly.express as px
import numpy as np
import pandas as pd
import os
from dotenv import load_dotenv

load_dotenv()

response_maxsize = 100000
default_ds_name = "openmedic"

default_filter_col = SUPPORTED_DBS[default_ds_name]["filter_cols"][0]
default_group_col = SUPPORTED_DBS[default_ds_name]["group_cols"][-2]
default_level_choice = SUPPORTED_DBS[default_ds_name]["level_choices"][0]
ds_name_choices = [
    {"label": value["libelle"], "value": key} for key, value in SUPPORTED_DBS.items()
]
default_year = 2019
default_filter_values = [
    1,
    "Dm pour traitements, aide à la vie, aliments et pansements",
]
df = pd.DataFrame({"x": np.random.rand(100), "y": np.random.rand(100)})
default_figure = px.scatter(df, x="x", y="y")  # , color="species"
default_table = pd.DataFrame({"rien": [0]})

# set the year choices for the suported dbs # TODO: inelegant: we could read parquet metadata to get number of partition ie number of years.
datasets_years = {}
for ds_name in SUPPORTED_DBS.keys():
    # opendamir has not yet been processed with partition (but could easily with a run data.compile)
    if ds_name == "opendamir":
        continue
    dataset_path = FS.ls(FS.ls(os.path.join(DIR2INTERIM, ds_name))[0])
    dataset_path = [l for l in dataset_path if l.rfind("=") != -1]
    datasets_years[ds_name] = [int(folder.split("=")[1]) for folder in dataset_path]
    datasets_years[ds_name].sort()
    datasets_years[ds_name] = [
        {"label": str(y).capitalize(), "value": y} for y in datasets_years[ds_name]
    ]
DEBUG_APP = os.getenv("DEBUG_APP", True)
DEBUG_TEST_DATA = os.getenv("DEBUG_TEST_DATA", False)

# Information on datasets
data_information = {}
for ds_name in SUPPORTED_DBS.keys():
    with open(os.path.join(DIR2DATA_INFORMATION, f"{ds_name}.md"), "r") as f:
        md_strings = f.read()
    data_information[ds_name] = md_strings
# home
with open(os.path.join(DIR2ASSETS, "home.md"), "r") as f:
    home_page_md = f.read()
encoded_image = base64.b64encode(
    open(os.path.join(DIR2ASSETS, "arbre_decisionnel.png"), "rb").read()
)
# UX
DEFAULT_PALETTE = sns.color_palette()
# if 20 categories are needed : sns.color_palette("tab20c")
DASHBOARD_PAGENAME = "dashboard"
PAGENAME_HOME = "home"
GUIDE_PAGENAME = "guide_utilisateur"
DATAGUIDE_PAGENAME = "datamap"
DATAAPP_PAGENAME = "dataapp"
RAPPORT_PAGENAME = "rapport"
ABOUT_PAGENAME = "about"
PAGE_NAMES = [
    DASHBOARD_PAGENAME,
    GUIDE_PAGENAME,
    DATAGUIDE_PAGENAME,
    DATAAPP_PAGENAME,
    ABOUT_PAGENAME,
]
PAGE_IDS = [f"{p_name}-link" for p_name in PAGE_NAMES]

# Error tracking
SENTRY_DSN = os.getenv("SENTRY_DSN")
REQUESTS_CA_BUNDLE = os.getenv("REQUESTS_CA_BUNDLE")
