from pathlib import Path
import json
from opensnds.app.utils.front_utils import drawFigure
import plotly.graph_objects as go
import pandas as pd
from plotly.subplots import make_subplots
from pandas.core.dtypes.missing import na_value_for_dtype
from opensnds.constants import (
    DNB,
    NB_CONSOMMANTS,
    REM,
    DNB_LABEL,
    NB_CONSOMMANTS_LABEL,
    NB_CONSOMMANTS_LABEL_BORNE_HAUTE,
    REM_LABEL,
    VALUE_COLS,
    DIR2INTERIM,
    DIR2TEST_DATA,
)
from opensnds.app.constants import FS

import os
from opensnds.config import SUPPORTED_DBS
from typing import Dict, List

import logging

PARQUET_ENGINE = os.getenv("PARQUET_TO_DATAFRAME_ENGINE", "pandas")

from opensnds.app.constants import (
    default_filter_col,
    default_ds_name,
    default_figure,
    FS,
)

import re
from opensnds.preprocessing.schema_handler import (
    get_nomenclature,
    join_nomenclatures,
    group_lines,
)


def break_line(x, max_len=10):
    paragraph = x.split(" ")
    lines = ""
    for i in range(0, len(paragraph), max_len):
        lines += " ".join(paragraph[i : i + max_len]) + "\n"
    return lines.strip()


def return_nomenclature_choices(filter_col, ds_name, schemas):
    if filter_col is None:
        filter_col = default_filter_col
    if ds_name is None:
        ds_name = default_ds_name
    nomenclature, primary_key = get_nomenclature(filter_col, schemas[ds_name])
    # label_col = list(nomenclature.columns[nomenclature.columns.str.contains(
    #     "LIB$|lib$|Lib")])
    label_col = []
    for col_nom in nomenclature.columns:
        if re.search("LIB$|lib$|^lib", col_nom.lower()) is not None:
            label_col.append(col_nom)
    if filter_col == "CIP13":
        # Exception for the irpha lib
        label_col = ["PHA_MED_NOM"]
    if len(label_col) != 0:
        nomenclature_choice = nomenclature[label_col[:1] + primary_key]
    else:
        nomenclature_choice = nomenclature[primary_key]

    nomenclature_choice = [
        {"label": f"{v}" + " : " + l.capitalize(), "value": v}
        for (l, v) in zip(
            nomenclature_choice.iloc[:, 0], nomenclature_choice.iloc[:, 1]
        )
    ]
    return nomenclature_choice


def tradSQL(
    ds_name: str,
    filter_col: str = None,
    filter_values: List[str] = None,
    group_cols: List[tuple] = None,
    years: List[str] = None,
    terminologie: str = None,
):
    """Construit depuis les éléments de l'API, une requête SQL valide sur le portail du SNDS.

    Args:
        ds_name (str): [description]
        filter_col (str, optional): [description]. Defaults to None.
        filter_values (List[str], optional): [description]. Defaults to None.
        group_cols (List[tuple], optional): [description]. Defaults to None.
        years (List[str], optional): [description]. Defaults to None.
        terminologie (str, optional): [description]. Defaults to None.

    Returns:
        [type]: [description]
    """
    join_9_keys_snds = """
        a.FLX_DIS_DTD = b.FLX_DIS_DTD
        and a.FLX_TRT_DTD = b.FLX_TRT_DTD
        and a.FLX_EMT_TYP = b.FLX_EMT_TYP
        and a.FLX_EMT_NUM = b.FLX_EMT_NUM
        and a.FLX_EMT_ORD = b.FLX_EMT_ORD
        and a.ORG_CLE_NUM = b.ORG_CLE_NUM
        and a.DCT_ORD_NUM = b.DCT_ORD_NUM
        and a.PRS_ORD_NUM = b.PRS_ORD_NUM
        and a.REM_TYP_AFF = b.REM_TYP_AFF
    """
    if ds_name is None:
        return "Veuillez renseigner un dataset"
    elif ds_name == "openmedic":
        # https://documentation-snds.health-data-hub.fr/fiches/medicament.html#cibler-des-patients-sur-une-classe-atc
        filterSQL = ""
        if terminologie is not None and filter_values is not None:
            filter_values = [str(v) for v in filter_values]
            terminologieATC = ""
            if terminologie.startswith("ATC"):
                if terminologie[-1] == "1":
                    terminologieATC = "1"
                elif terminologie[-1] == "2":
                    terminologieATC = "3"
                elif terminologie[-1] == "3":
                    terminologieATC = "4"
                elif terminologie[-1] == "4":
                    terminologieATC = "5"
                elif terminologie[-1] == "5":
                    terminologieATC = "6"
                filterSQL = "and SUBSTR(d.PHA_ATC_C07,1,{}) IN ('{}') ".format(
                    terminologieATC, "', '".join(filter_values)
                )
            elif terminologie.startswith("CIP"):
                filterSQL = "and d.PHA_CIP_C13 IN ({}) ".format(
                    "', '".join(filter_values)
                )
        timeSQL = getTimeSQL(years)
        groupSQL = getGroupBySQL(group_cols)
        query = """SELECT
            COUNT(*) AS denombrement,
            COUNT(DISTINCT(a.BEN_NIR_PSA)) AS nb_patients,
            SUM(b.PHA_ATC_QSN * b.PHA_ACT_PRU) AS montant_rembourse,
            year(a.EXE_SOI_DTD) AS annee
            FROM ER_PRS_F a,
            ER_PHA_F b,
            IR_PHA_R c
            WHERE TRIM(c.PHA_CIP_C13) = TRIM(b.PHA_PRS_C13) and
            {}
            {}
            {}
            {}
            """.format(
            join_9_keys_snds, filterSQL, timeSQL, groupSQL
        )
    elif ds_name == "openbio":
        filterSQL = ""
        if filter_values is not None:
            filter_values = [str(v) for v in filter_values]
            if terminologie == "ACTE":
                filterSQL = "and b.BIO_PRS_IDE IN ({}) ".format(
                    "', '".join(filter_values)
                )
            elif terminologie == "GRP":
                filterSQL = "and c.BIO_PHY_GRP IN ({}) ".format(
                    "', '".join(filter_values)
                )
        timeSQL = getTimeSQL(years)
        groupSQL = getGroupBySQL(group_cols)
        query = """SELECT
            COUNT(*) AS denombrement,
            COUNT(DISTINCT(a.BEN_NIR_PSA)) AS nb_patients,
            SUM(a.PRS_ACT_QTE * (a.BSE_REM_MNT + a.CPL_REM_PRU) AS montant_rembourse,
            year(a.EXE_SOI_DTD) AS annee
            FROM er_prs_f a,
            er_bio_f b,
            ir_bio_r c
            WHERE
            c.BIO_PRS_IDE = b.BIO_PRS_IDE
            {}
            {}
            {}
            {}
            """.format(
            join_9_keys_snds, filterSQL, timeSQL, groupSQL
        )
    elif ds_name == "openlpp":
        filterSQL = ""
        if filter_values is not None:
            filter_values = [str(v) for v in filter_values]
            filterSQL = "and b.TIP_PRS_IDE IN ({}) ".format("', '".join(filter_values))
        timeSQL = getTimeSQL(years)
        groupSQL = getGroupBySQL(group_cols)
        query = """SELECT
            COUNT(*) AS denombrement,
            COUNT(DISTINCT(a.BEN_NIR_PSA)) AS nb_patients,
            SUM(b.TIP_ACT_QSN * b.TIP_ACT_PRU) AS montant_rembourse,
            year(a.EXE_SOI_DTD) AS annee
            FROM er_prs_f a,
            er_tip_f b
            WHERE
            {}
            {}
            {}
            {}
            """.format(
            join_9_keys_snds, filterSQL, timeSQL, groupSQL
        )
    else:
        query = "dataset " + ds_name + " non supporté"
    return query


def getTimeSQL(years: int):
    if years is None:
        return ""
    return (
        "and a.EXE_SOI_DTD between to_date('0101"
        + str(years)
        + "','ddmmyyyy') and to_date('3112"
        + str(years)
        + "','ddmmyyyy')"
    )


def getGroupBySQL(group_cols: List):
    if group_cols is None:
        return ""
    groupSQL = "GROUP BY "
    for col_cod in group_cols:
        if "DTD" in col_cod[1] or "DTF" in col_cod[1] or "DTE" in col_cod[1]:
            groupSQL = f"{groupSQL}{col_cod[2]}({col_cod[0]}.{col_cod[1]}),"
        else:
            groupSQL = f"{groupSQL}{col_cod[0]}.{col_cod[1]},"
    return groupSQL[:-1]


def encapsSAS(ds_name: str, querySQL: str):
    return f"PROC SQL ; CREATE TABLE requete_opensnds_{ds_name} AS {querySQL}; QUIT"


def query_table(
    ds_name: str,
    dataset: pd.DataFrame,
    ds_schema: Dict[str, Dict],
    group_cols: List[str],
    filter_col: str = None,
    filter_values: List[str] = None,
    years: List[str] = None,
) -> pd.DataFrame:
    """
    Query method that can be used for all datasets. Configuration is stored into the config.SUPPORTED_DBS dictionnary
    """
    df = dataset
    if (filter_col is not None) & (filter_values is not None):
        if len(filter_values) != 0:
            if df.dtypes[filter_col] == object:
                filter_values = [str(x) for x in filter_values]
                df = df[df[filter_col.upper()].isin(filter_values)]
            else:
                filter_values = [float(x) for x in filter_values]
                df = df[df[filter_col.upper()].isin(filter_values)]
    if years is not None:
        frequency_col = str(SUPPORTED_DBS[ds_name]["frequency"][0])
        if not isinstance(years, list):
            years = [years]
        years = [str(y) for y in years]
        if len(years) == 1:
            query = "{frequency_col} == {years}".format(
                frequency_col=frequency_col, years=years[0]
            )
        else:
            query = "{frequency_col} in ({years})".format(
                frequency_col=frequency_col, years=", ".join(years)
            )
        df = df.query(query)
    if not isinstance(group_cols, list):
        group_cols = [group_cols]

    if PARQUET_ENGINE == "pandas":
        response = (
            df.groupby(group_cols)
            .agg(
                **{
                    DNB: pd.NamedAgg(SUPPORTED_DBS[ds_name][DNB], "sum"),
                    REM: pd.NamedAgg(SUPPORTED_DBS[ds_name][REM], "sum"),
                    NB_CONSOMMANTS: pd.NamedAgg(
                        SUPPORTED_DBS[ds_name][NB_CONSOMMANTS], "sum"
                    ),
                }
            )
            .reset_index()
        )
    elif PARQUET_ENGINE == "koalas":
        response = (
            df.groupby(group_cols)
            .agg(
                **{
                    DNB: ks.NamedAgg(SUPPORTED_DBS[ds_name][DNB], "sum"),
                    REM: ks.NamedAgg(SUPPORTED_DBS[ds_name][REM], "sum"),
                    NB_CONSOMMANTS: ks.NamedAgg(
                        SUPPORTED_DBS[ds_name][NB_CONSOMMANTS], "sum"
                    ),
                }
            )
            .reset_index()
        )
        response = response.to_pandas()
    else:
        raise ValueError("Lecteur de Parquet non pris en charge")
    response_l = join_nomenclatures(response, group_cols, ds_schema)
    try:
        response_l = group_lines(response_l, na_value_for_dtype, "Inconnu")
    except Exception as err:
        logging.warn(f"Unexpected {err=}, {type(err)=}")
    for col in response_l.columns:
        if col not in [DNB, NB_CONSOMMANTS, REM]:
            response_l[col] = response_l[col].astype("category")
    return response_l


#
def load_datasets(debug=False):
    if debug:
        datasets = {}
        for dsname in SUPPORTED_DBS.keys():
            if PARQUET_ENGINE == "koalas":
                datasets[dsname] = ks.read_parquet(
                    os.path.join(DIR2TEST_DATA, "interim", dsname)
                )
            elif PARQUET_ENGINE == "pandas":
                datasets[dsname] = pd.read_parquet(
                    os.path.join(DIR2TEST_DATA, "interim", dsname),
                    engine="pyarrow",
                    filesystem=FS,
                )
            else:
                raise ValueError("Lecteur de Parquet non pris en charge")
    else:
        datasets = {}
        for dsname in SUPPORTED_DBS.keys():
            if SUPPORTED_DBS[dsname]["consommants"]:
                for sub_db_path in FS.ls(os.path.join(DIR2INTERIM, dsname)):
                    sub_db_name = Path(sub_db_path).stem
                    sub_db_path = os.path.join(DIR2INTERIM, dsname, sub_db_name)
                    if PARQUET_ENGINE == "koalas":
                        datasets[dsname + "__" + sub_db_path] = ks.read_parquet(
                            sub_db_path
                        )
                    elif PARQUET_ENGINE == "pandas":
                        datasets[dsname + "__" + sub_db_name] = pd.read_parquet(
                            sub_db_path, engine="pyarrow", filesystem=FS
                        )
                    else:
                        raise ValueError("Lecteur de Parquet non pris en charge")
            else:
                if PARQUET_ENGINE == "koalas":
                    ds_path = os.path.join(DIR2INTERIM, dsname)
                    datasets[dsname] = ks.read_parquet(ds_path)
                elif PARQUET_ENGINE == "pandas":
                    datasets[dsname] = pd.read_parquet(
                        ds_path, engine="pyarrow", filesystem=FS
                    )
                else:
                    raise ValueError("Lecteur de Parquet non pris en charge")
    return datasets


# plot functions for dashboard


def get_time_barplot(
    ds_name, dataset, ds_schema, group_cols, filter_col, filter_values, years=None
):
    df = (
        query_table(
            ds_name,
            dataset,
            ds_schema,
            group_cols,
            filter_col=filter_col,
            filter_values=filter_values,
            years=None,
        )
        .set_index(VALUE_COLS)
        .reset_index()
    )
    if group_cols == "year_month":
        # parse date if year_month format
        df[group_cols] = pd.to_datetime(
            df[group_cols].map(lambda x: pd.to_datetime(x, format="%Y%m"))
        )
    else:
        # else year
        df[group_cols] = df[group_cols].astype(int)

    df = df.sort_values(df.columns[-1])
    if len(df) == 0:
        fig = default_figure
        df = pd.DataFrame({"Pas de données": [0]})
        return fig, df
    fig = make_subplots(rows=1, cols=1, specs=[[{"secondary_y": True}]])
    color1 = "#034eca"
    color2 = "#ff929a"
    fig.add_trace(
        go.Bar(
            x=df.iloc[:, -1].astype(str),
            y=df[REM],
            marker_color=color2,
            yaxis="y1",
            name=REM_LABEL,
            opacity=0.3,
            width=0.5,
        ),
        row=1,
        col=1,
        secondary_y=True,
    )
    if SUPPORTED_DBS[ds_name]["consommants"]:
        if filter_values is not None:
            if len(filter_values) > 1:
                fig.add_trace(
                    go.Scatter(
                        x=df.iloc[:, -1].astype(str),
                        y=df[NB_CONSOMMANTS],
                        marker_color=color1,
                        yaxis="y2",
                        name=NB_CONSOMMANTS_LABEL_BORNE_HAUTE,
                        line_width=5,
                        line_dash="dot",
                        marker_size=5,
                    ),
                    row=1,
                    col=1,
                    secondary_y=False,
                )
            else:
                fig.add_trace(
                    go.Scatter(
                        x=df.iloc[:, -1].astype(str),
                        y=df[NB_CONSOMMANTS],
                        marker_color=color1,
                        yaxis="y2",
                        name=NB_CONSOMMANTS_LABEL,
                        line_width=5,
                        line_dash="dot",
                        marker_size=5,
                    ),
                    row=1,
                    col=1,
                    secondary_y=False,
                )
        y1_title = "Nombre"
    else:
        y1_title = "Nombre"
    fig.add_trace(
        go.Scatter(
            x=df.iloc[:, -1].astype(str),
            y=df[DNB],
            marker_color=color1,
            yaxis="y2",
            name=DNB_LABEL,
            line_width=5,
            marker_size=5,
        ),
        row=1,
        col=1,
        secondary_y=False,
    )

    ymax_actes = max(df[DNB])
    ymax_actes += ymax_actes / 20
    ymax_rem = max(df[REM])
    fig.update_layout(
        plot_bgcolor="#f4f3f3",
        font_family="Raleway",
        showlegend=True,
        legend=dict(y=0.99, x=0.01, yanchor="top", xanchor="left"),
        yaxis=dict(
            title=y1_title,
            titlefont=dict(color=color1),
            tickfont=dict(color=color1),
            range=[0, ymax_actes],
            anchor="free",
            side="left",
            position=0,
            showgrid=False,
        ),
        yaxis2=dict(
            title="Montants (€)",
            showgrid=False,
            titlefont=dict(color=color2),
            tickfont=dict(color=color2),
            range=[0, ymax_rem],
            anchor="free",
            side="right",
            position=0.95,
        ),
        margin={"t": 0, "l": 0, "r": 0, "b": 0},
    )
    return fig, df.round(2)


def get_group_histogram(
    ds_name, dataset, ds_schema, group_cols, filter_col, filter_values, years=None
):
    if filter_values is not None:
        query_codes = ", ".join([str(c) for c in filter_values])
    else:
        query_codes = "Inconnu"
    # TODO: Pour les régions, on voudrait certainement que ce soit classé par nombre d'actes (et également avoir plutôt un rapport/ nb hab)
    df = (
        query_table(
            ds_name,
            dataset,
            ds_schema,
            group_cols,
            filter_col=filter_col,
            filter_values=filter_values,
            years=years,
        )
        .set_index(VALUE_COLS)
        .reset_index()
    )
    df = df.sort_values(df.columns[-1])
    if len(df) == 0:
        fig = default_figure
        df = pd.DataFrame({"Pas de données": [0]})
        return fig, df
    fig = make_subplots(rows=1, cols=1, specs=[[{"secondary_y": True}]])
    color1 = "#034eca"
    color2 = "#ff929a"
    fig.add_trace(
        go.Bar(
            x=df.iloc[:, -1].astype(str),
            y=df[DNB],
            marker_color=color1,
            yaxis="y1",
            name=DNB_LABEL,
            opacity=0.4,
            width=0.5,
        ),
        row=1,
        col=1,
        secondary_y=False,
    )
    if SUPPORTED_DBS[ds_name]["consommants"]:
        if filter_values is not None:
            fig.add_trace(
                go.Scatter(
                    x=df.iloc[:, -1].astype(str),
                    y=df[NB_CONSOMMANTS],
                    marker_line_color=color1,
                    yaxis="y1",
                    name="Consommants",
                    marker_size=20,
                    mode="markers",
                    marker_symbol=41,
                    marker_line_width=10,
                ),
                row=1,
                col=1,
                secondary_y=False,
            )
        y1_title = "Nombre"
    else:
        y1_title = "Nombre"
    fig.add_trace(
        go.Scatter(
            x=df.iloc[:, -1].astype(str),
            y=df[REM],
            marker_line_color=color2,
            yaxis="y2",
            name=REM_LABEL,
            marker_size=20,
            mode="markers",
            marker_symbol=41,
            marker_line_width=10,
        ),
        row=1,
        col=1,
        secondary_y=True,
    )
    ymax_actes = max(df[DNB])
    ymax_actes += ymax_actes / 20
    ymax_rem = max(df[REM])
    fig.update_layout(
        plot_bgcolor="#f4f3f3",
        font_family="Raleway",
        showlegend=True,
        legend=dict(y=0.99, x=0.01, yanchor="top", xanchor="left"),
        yaxis=dict(
            title=y1_title,
            titlefont=dict(color=color1),
            tickfont=dict(color=color1),
            anchor="free",
            # overlaying="y",
            range=[0, ymax_actes],
            side="left",
            position=0,
            showgrid=False,
        ),
        yaxis2=dict(
            title="Montants (€)",
            titlefont=dict(color=color2),
            tickfont=dict(color=color2),
            range=[0, ymax_rem],
            anchor="free",
            side="right",
            position=0.95,
            showgrid=False,
        ),
        margin={"t": 0, "l": 0, "r": 0, "b": 0},
    )
    return fig, df.round(2)
