# These functions serves formatted objects to the front

from typing import List

import dash_table
from dash_table.Format import Format, Group, Align, Scheme

import dash_bootstrap_components as dbc
import dash_html_components as html

import dash_core_components as dcc

from opensnds.constants import (
    NB_CONSOMMANTS,
    DNB,
    REM,
    NB_CONSOMMANTS_LABEL,
    NB_CONSOMMANTS_LABEL_BORNE_HAUTE,
    DNB_LABEL,
    REM_LABEL,
)


def nice_dash_table(
    df, id: str, column_name_mapping: List = None, display_nb_consommants: int = 0
):
    """
    When included in dash_bootsrap component, following css should be included into assets/<filename>.css to take into account
    the dash_table styling, especially the column overflow and the left-margin at 0
    ```
    .dash-table-container .row {
        display: block;
        margin: 0;
    }
    ```
    """
    if column_name_mapping is None:
        column_name_mapping = []
        for i in df.columns:
            if i == "year":
                column_name_mapping.append(
                    {"name": "Années", "id": i, "format": Format(align=Align.left)}
                )
            elif i == "SEX_LIB":
                column_name_mapping.append(
                    {"name": "Sexes", "id": i, "format": Format(align=Align.left)}
                )
            elif i == "AGE_BEN_LIB":
                column_name_mapping.append(
                    {
                        "name": "Classes d'âge",
                        "id": i,
                        "format": Format(align=Align.left),
                    }
                )
            elif i == "BEN_RES_LIB":
                column_name_mapping.append(
                    {"name": "Régions", "id": i, "format": Format(align=Align.left)}
                )
            elif i == "PSP_SPE_LIB":
                column_name_mapping.append(
                    {"name": "Spécialité du prescripteur", "id": i, "format": Format(align=Align.left)}
                )
            elif i == DNB:
                column_name_mapping.append(
                    {
                        "name": DNB_LABEL,
                        "id": i,
                        "type": "numeric",
                        "format": Format(
                            scheme=Scheme.fixed,
                            precision=0,
                            group=Group.yes,
                            groups=3,
                            group_delimiter=" ",
                        ),
                    }
                )
            elif i == NB_CONSOMMANTS:
                if display_nb_consommants == 1:
                    column_name_mapping.append(
                        {
                            "name": NB_CONSOMMANTS_LABEL,
                            "id": i,
                            "type": "numeric",
                            "format": Format(
                                scheme=Scheme.fixed,
                                precision=0,
                                group=Group.yes,
                                groups=3,
                                group_delimiter=" ",
                            ),
                        }
                    )
                elif display_nb_consommants > 1:
                    column_name_mapping.append(
                        {
                            "name": NB_CONSOMMANTS_LABEL_BORNE_HAUTE,
                            "id": i,
                            "type": "numeric",
                            "format": Format(
                                scheme=Scheme.fixed,
                                precision=0,
                                group=Group.yes,
                                groups=3,
                                group_delimiter=" ",
                            ),
                        }
                    )
            elif i == REM:
                column_name_mapping.append(
                    {
                        "name": REM_LABEL,
                        "id": i,
                        "type": "numeric",
                        "format": Format(
                            scheme=Scheme.fixed,
                            precision=2,
                            group=Group.yes,
                            groups=3,
                            group_delimiter=" ",
                            decimal_delimiter=",",
                        ),
                    }
                )
            else:
                column_name_mapping.append(
                    {
                        "name": " ".join(i.lower().split("_")).title(),
                        "id": i,
                        "type": "numeric",
                        "format": Format(
                            scheme=Scheme.fixed,
                            precision=2,
                            group=Group.yes,
                            groups=3,
                            group_delimiter=" ",
                            decimal_delimiter=",",
                        ),
                    }
                )
        column_name_mapping.insert(
            0, column_name_mapping.pop(len(column_name_mapping) - 1)
        )
    nice_table = dash_table.DataTable(
        id="table",
        columns=column_name_mapping,
        data=df.to_dict("records"),
        filter_action="native",
        sort_action="native",
        sort_mode="single",
        style_data={"whiteSpace": "normal", "height": "auto"},  # overflow strategy
        style_data_conditional=[
            {"if": {"row_index": "odd"}, "backgroundColor": "#f4f3f3"}
        ],
        style_header={"backgroundColor": "#f4f3f3", "fontWeight": "bold"},
        style_cell={
            "font_family": "Raleway",
            "textOverflow": "ellipsis",
            "whiteSpace": "normal",
            "height": "auto",  # overflow strategy
        },
        style_as_list_view=True,
        page_size=20,
        style_table={"maxHeight": "100%", "overflowY": "scroll"},
    )
    return nice_table


def drawFigure(fig, id=None):
    if id is None:
        id = ""
    return html.Div(
        [
            dbc.Card(
                dbc.CardBody(
                    [
                        dcc.Graph(
                            figure=fig.update_layout(
                                margin={"t": 0, "l": 0, "r": 0, "b": 0},
                                showlegend=False,
                            ),
                            # template='plotly_dark',
                            # plot_bgcolor='rgba(0, 0, 0, 0)',
                            # paper_bgcolor='rgba(0, 0, 0, 0)'),
                            id=id,
                            # config={'displayModeBar': False}
                        )
                    ]
                )
            )
        ]
    )
