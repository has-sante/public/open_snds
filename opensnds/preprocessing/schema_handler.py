import logging
import json
import os
import re
import sys

from typing import List, Tuple
from tableschema.schema import Schema
import pandas as pd
from tableschema.table import Table

from opensnds.config import SUPPORTED_DBS
from opensnds.constants import (
    DIR2SCHEMAS,
    ROOT_DIR,
    SCHEMA_SNDS_GITLAB_RAW_URL,
    DIR2NOMENCLATURES,
    NOMENCLATURES,
    FS,
)


from opensnds.data.compile import download_nomenclature_by_name


def load_schema(db_name: str) -> Schema:
    schema = json.load(FS.open(f"{DIR2SCHEMAS}/{db_name}.json", "r"))
    return Schema(schema)


def load_schemas():
    schemas = {}
    for dbname in SUPPORTED_DBS.keys():
        try:
            schemas[dbname] = load_schema(dbname)
        except Exception:
            logging.warn("Unexpected error:", sys.exc_info()[0])
            logging.warn(f"No schema found for {dbname}, manually try to redownload ")
    return schemas


def recover_nomenclature(nomenclature_name, verify=False):
    success = False
    for source in ["ORAREF", "ORAVAL", "ATIH", "DREES", "OPENDAMIR"]:
        nomenclatures_url = f"{SCHEMA_SNDS_GITLAB_RAW_URL}/nomenclatures/{source}/"
        success = download_nomenclature_by_name(
            nomenclature_name, nomenclatures_url, verify
        )
        if success:
            logging.info("Nomenclature found")
            return
    logging.info("Nomenclature not found")


# Fonction initialement crée pour replacer les NaN dans le libellé inconnu
def group_lines(df: pd.DataFrame, line_to_delete: any, line_to_add: str):
    line_to_add_label = line_to_add
    # Pour récupérer la colonne avec le terme "inconnu"
    for col_nom in df.columns:
        if (
            re.search(f"{line_to_add}$|{line_to_add}$|^{line_to_add}", col_nom.lower())
            is not None
        ):
            line_to_add_label = col_nom
    # Pour rechercher les lignes dans toutes les colonnes
    for col_nom in df.columns:
        if re.search("LIB$|lib$|^lib", col_nom.lower()) is not None:
            df.set_index(col_nom)
            df = (
                df.replace({col_nom: {line_to_delete: line_to_add_label}})
                .groupby(col_nom, sort=False)
                .sum()
            )
            df.reset_index(drop=False, inplace=True)
    return df


def join_nomenclatures(
    df: pd.DataFrame,
    columns_to_get: List[str],
    dbschema: Schema,
    drop_primarykey_cols: bool = True,
):
    """Join opensnds nomenclatures to an opensnds table given some columns for which to search nomenclatures.
    Args:
        df (pd.DataFrame): [description]
        columns_to_get (List[str]): [description]
        dbschema (Schema): [description]
        drop_primarykey_cols (bool, optional): [description]. Defaults to True.

    Returns:
        [type]: [description]
    """
    dropped_cols = []
    # vérifie que les colonnes demandées sont uniques
    columns_to_get = set(columns_to_get)
    for col in columns_to_get:
        if col in dbschema.field_names:
            nomenclature, primary_key = get_nomenclature(col, dbschema)
            # if lib field found, return only lib (we might want other fields in the nomenclatures but we will make specific developments for each nomenclature in this case)
            if nomenclature is None:
                continue
            label_col = []
            for col_nom in nomenclature.columns:
                if re.search("LIB$|lib$|^lib", col_nom.lower()) is not None:
                    label_col.append(col_nom)

            if len(label_col) != 0:
                nomenclature = nomenclature.loc[:, label_col + primary_key]
            # force same type for join cols if int
            nomenclature.loc[:, primary_key] = nomenclature.loc[:, primary_key].astype(
                df.dtypes[col]
            )
            df = df.merge(nomenclature, left_on=col, right_on=primary_key, how="left")
            if len(nomenclature) != 0:
                dropped_cols += primary_key
                dropped_cols += [col]
    if drop_primarykey_cols:
        df = df.drop(set(dropped_cols), axis=1)
    return df


def get_nomenclature(variable_name: str, dbschema: Schema) -> Tuple[pd.DataFrame, str]:
    try:
        field = dbschema.get_field(variable_name)
        if field is None:
            logging.warn("Nomenclature introuvable pour : " + variable_name)
            return
        nomenclature_name = field.descriptor["nomenclature"]
        if nomenclature_name != "-":
            nomenclature, primary_key = get_nomenclature_from_name(nomenclature_name)
        else:
            nomenclature = None
            primary_key = [variable_name]
    except KeyError:
        logging.warn(
            "Pas de champ nommé {} dans le schéma {}".format(variable_name, dbschema)
        )
        nomenclature = None
        primary_key = [variable_name]
    return nomenclature, primary_key


def get_nomenclature_from_name(nomenclature_name):
    # if not os.path.isfile(path2nomenclature_json):
    # logging.info("Trying tor recover nomenclature from gitlab...")
    # recover_nomenclature(nomenclature_name)
    return (
        NOMENCLATURES[nomenclature_name]["data"],
        NOMENCLATURES[nomenclature_name]["primary_key"],
    )


def add_missing_nomenclature_from_doc(
    dbname: str,
):
    schema = load_schema(dbname)
    path2documentation = os.path.join(
        ROOT_DIR, "documentation", f"Lexique_{dbname}.xls"
    )
    documentation = pd.read_excel(path2documentation, sheet_name=None)
    for field in schema.fields:
        field_name = field.name
        if (field.descriptor["nomenclature"] == "-") & (
            field_name in documentation.keys()
        ):
            nomenclature = documentation[field_name].iloc[:, :2]
            nomenclature.rename(
                columns={nomenclature.columns[0]: "Lib " + nomenclature.columns[0]}
            )
            nomenclature_name = f"{dbname}_{field_name}"
            # FIXME: This is probably broken on non-local filesystems.
            nomenclature_path = os.path.join(DIR2NOMENCLATURES, f"{nomenclature_name}")
            nomenclature.to_csv(nomenclature_path + ".csv", index=False)
            # logic to create nomenclature schema
            nomenclature_table = Table(nomenclature_path + ".csv")
            nomenclature_table.read()
            nomenclature_table.infer()
            nomenclature_schema = nomenclature_table.schema
            nomenclature_schema.descriptor["name"] = nomenclature_name
            nomenclature_schema.descriptor["title"] = nomenclature.columns[1]
            nomenclature_schema.descriptor["primaryKey"] = nomenclature.columns[0]
            nomenclature_schema.commit()
            nomenclature_schema.save(nomenclature_path + ".json")
        # update schema
        field.descriptor["nomenclature"] = nomenclature_name
        schema.update_field(field_name, field.descriptor)
    schema.commit()
    schema.save(os.path.join(DIR2SCHEMAS, f"{dbname}.json"))
