# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown] {"tags": []}
# # Evaluation de la qualité des données ouvertes mises à disposition par l'Assurance Maladie
#
# ## Introduction
#
# L'Assurance Maladie met à disposition des données ouvertes.
# Ces données concernent le remboursement en ville des médicaments, des dispositifs médicaux, ou encore des actes de biologie. Ces données sont livrées agrégées sur plusieurs variables relatives au bénéficiaire telles que sa tranche d'âge ou son lieu de résidence.
#
# Nous mettons à disposition des utilisateurs un accès facilité à ces données par le biais d'une application web. Il convient donc de vérifier au préalable la qualité de ces données par rapport aux données du pportail SNDS pour assurer à l'utilisateur l'exactitude des résultats ou de lui avertir d'un biais quantifiable le cas échéant.
#
# ## Methode
#
# L'étude reposera sur une comparaison entre les données fournis par l'Assurance Maladie et une analyse des données du portail SNDS.
#
# ### Mesures
#
# Il s'agira de quantifier les proportions d'écarts moyens et maximum oberservées entre le jeu de données ouvertes et les données du portail SNDS pour différents comptages :
# - Nombre de bénéficaire
# - Nombre d'actes
# - Somme des remboursements
#
# Ces comparaisons seront limitées aux jeux de données suivant :
# - OpenMedic
# - OpenBio
# - OpenLPP
#
# Ces chiffrages se feront en valeur absolue mais indiqueront le sens de l'écart (positif ou négatif entre les deux jeux de données).
# Si un écart conséquent est mesuré entre les jeux de données, une hypothèse permettant d'expliquer au moins en partie cet écart sera énoncée.
#
# ### Requêtes effectuées
#
# Les requêtes effectuées sur le portail se feront sur les données DCIR.
# Elles reproduiront les mêmes aggrégations que pour les données présentes dans les jeu de données mises à disposition par l'Assurance Maladie.
#
# Les requêtes porteront sur 3 années différentes.
# Une cinquantaine de codes seront interogées à partir des terminologies mises à disposition.
#
# Les régles de nettoyage du SNDS seront repectées, à savoir :
#  - `DPN_QLF ≠ "71"` (suppression des lignes pour remontée d'information : ces lignes correspondent à des prestations effectuées lors d'un séjour ou d'une consultation ambulatoire et envoyées à titre indicatif)
#  - `BSE_PRS_NAT ≠ "0"` (suppression des lignes pour lesquelles la nature de la prestation est sans objet)
#
# Les requêtes complètes sous forme de procédure SAS sont disponibles à la fin du document en annexe.
#
# NB: Les requêtes ont été découpées sur plusieurs mois pour pouvoir aboutir sur le portail.
#
# ## Resultats
#
# Les résultats indiquent les écarts entre les valeurs données par les données ouvertes et le SNDS.
#
# ### Nombre de bénéficiaires : analgésiques sur l'année 2019
#
# Les critères de filtrage de la requête portent sur les codes ATC correspondant aux analgésiques sont récupérées avec une jointure sur la table de référence `IR_PHA_R` avec une clause LIKE `PHA_ATC_C07 LIKE 'N02%'`. Le calcul du nombre de bénéficiaires se fait ensuite par sexe et code ATC de niveau 5.
#
# | Variable  | Valeur |
# | :--------------- | -----:|
# | Nombre de codes  |  26  |
# | Différence de proportion moyenne  | 18400% |
# | Proportion de différence médiane  | 9% |
# | Proportion de différence maximale  | 682 023.75% |
#
# ### Nombre de bénéficiaires : codéine par codes CIP sur l'année 2019
#
# Les critères de filtrage de la requête portent sur les codes CIP13 (plus petit niveau d'agrégation possible) en récupérant tout les codes CIP13 relatives à la codéine par l'intérmédiaire d'une jointure sur la table de référence `IR_PHA_R` avec une clause LIKE `PHA_ATC_L07 LIKE '%CODEINE%'`. Le calcul du nombre de bénéficiaires se fait ensuite par sexe et code CIP13.
#
# | Mesure | Valeur |
# | :--------------- | -----:|
# | Nombre de codes  |  11  |
# | Proportion de différence moyenne  | 3% |
# | Proportion de différence médiane  | 3,2% |
# | Proportion de différence maximale  | 10% |
#
# ### Nombre de bénéficiaires : pénicilines sur l'année 2018
#
# Les critères de filtrage de la requête portent sur les codes ATC correspondant aux analgésiques sont récupérées avec une jointure sur la table de référence `IR_PHA_R` avec une clause LIKE `PHA_ATC_C07 LIKE 'JO1C%'`. Le calcul du nombre de bénéficiaires se fait ensuite par sexe et code ATC de niveau 5.
#
# | Mesure  | Valeur |
# | :--------------- | -----:|
# | Nombre de codes  |  17  |
# | Différence de proportion moyenne  | -18% |
# | Proportion de différence médiane  | -15,3% |
# | Proportion de différence maximale  | -38% |
#
# Proportion de différence moyenne
#
# ### Nombre de bénéficiaires : dosage TSH sur année 2017
#
# Les critères de filtrage de la requête portent sur une liste de codes NABM correspondant aux dosages de TSH. Le calcul du nombre de bénéficiaires se fait ensuite par sexe et par code NABM.
#
# | Mesure  | Valeur |
# | :--------------- | -----:|
# | Nombre de codes  |  5  |
# | Proportion de différence moyenne  | 1% |
# | Proportion de différence médiane  | -1% |
# | Proportion de différence maximale  | 4,6% |
#
# ### Nombre de bénéficiaires : Produits et Prestations (DM) en 2019
#
# Les critères de filtrage de la requête portent sur une liste de codes LPP (Liste de Produits et Prestations). Le calcul du nombre de bénéficiaires se fait ensuite par sexe et par code LPP.
#
# | Mesure  | Valeur |
# | :--------------- | -----:|
# | Nombre de codes  |  44  |
# | Proportion de différence moyenne   | 117% |
# | Proportion de différence médiane  | 63% |
# | Proportion de différence maximale  | 2350% |
#
# ### Dénombrement et somme des remboursements : analgésiques sur l'année 2018
#
# Les critères de filtrage de la requête portent sur les codes ATC correspondant aux analgésiques sont récupérées avec une jointure sur la table de référence `IR_PHA_R` avec une clause LIKE `PHA_ATC_C07 LIKE 'N02%'`. Le taux de remboursements est pris en compte dans le calcul `SUM(b.PHA_ACT_QSN * b.PHA_ACT_PRU * (a.RGO_REM_TAU/100)`.
#
# Le calcul de la somme des remboursement et du dénombrement des prestations se fait ensuite par sexe et code ATC de niveau 5.
#
# #### Dénombrement
#
# | Mesure  | Valeur |
# | :--------------- | -----:|
# | Nombre de codes  |  16 |
# | Proportion de différence moyenne | 30000% |
# | Proportion de différence médiane  | 157% |
# | Proportion de différence maximale  | 958% |
#
# #### Somme des remboursements
#
# | Mesure  | Valeur |
# | :--------------- | -----:|
# | Nombre de codes  |  16  |
# | Différence moyenne  | NA |
# | Proportion de différence médiane  | 421% |
# | Proportion de différence maximale  | -9562% |
#
# ## Discussion
#
# ### Qualité générale des données
#
# On remarque une différence médiane sur tous les jeux de données OpenMedic d'environ 10% avec les codes ATC. La différence est beaucoup plus faible lorsqu'on travaille avec les codes CIP (3%).
# Les données OpenBio sont encore plus proches des données du portail (différence médiane de 1%).
# On constate cependant de grandes différences entre les chiffres de OpenLPP et les données du portail (différence médianne de 63%).
# On ne remarque pas de différences de qualité entre les différentes années, l'écart semble être dans le même ordre de grandeur.
#
# ### Hypothèses sur les différences importants de nombres de consommants pour les données OpenMedic
#
# Nous pouvons remarquer qu'il existe des différences importantes au niveau des comptages (consommants et somme des remboursements) pour les codes ATC de niveau 5. Or, ces écarts ne sont pas observables pour les codes CIP correspondant à ces codes ATC. Il est possible que les codes CIP ne soit pas assignées aux mêmes codes ATC dans la table de référence IR_PHA_R du portail SNDS que dans le jeu de données OpenMedic.
# Il est donc recommandé d'utiliser les codes CIP dans l'application OpenSnds.
#
# On constate un écart également sur certains codes LPP qui reste difficilement explicable.
#
# ## Conclusion
#
# Il existe une variation entre les dénombrements de bénéficiaires donnés dans open snds et les résultats des requêtes faites dans le portail. Cependant, si ce ne pas sont des écarts négligeables (10% en moyenne), les nombres donnés en open data permettent de donner un ordre de grandeur rapidement à un utilisateur sans que celui ci est besoin d'un accès au portail SNDS.
#
# ## Annexes
#
# ### Requêtes
#
# #### Analgésiques sur l'année 2019
#
# ```
# proc sql;
# %connectora;
# create table liste_patients2 as select * from connection to oracle
# (select
# 	COUNT(DISTINCT(a.BEN_NIR_PSA)) AS nb_patients,
# 	d.PHA_ATC_C07 AS ATC,
# 	a.BEN_SEX_COD AS sexe
# from ER_PRS_F a,
# ER_PHA_F b,
# IR_PHA_R d
# where a.FLX_DIS_DTD = b.FLX_DIS_DTD
# and a.FLX_TRT_DTD = b.FLX_TRT_DTD
# and a.FLX_EMT_TYP = b.FLX_EMT_TYP
# and a.FLX_EMT_NUM = b.FLX_EMT_NUM
# and a.FLX_EMT_ORD = b.FLX_EMT_ORD
# and a.ORG_CLE_NUM = b.ORG_CLE_NUM
# and a.DCT_ORD_NUM = b.DCT_ORD_NUM
# and a.PRS_ORD_NUM = b.PRS_ORD_NUM
# and a.REM_TYP_AFF = b.REM_TYP_AFF
# and trim(d.PHA_CIP_C13) = trim(b.PHA_PRS_C13)
# and (a.EXE_SOI_DTD between to_date('01/01/2019','dd/mm/yyyy') and to_date('31/12/2019','dd/mm/yyyy'))
# and (a.FLX_DIS_DTD between to_date('01/02/2019','dd/mm/yyyy') and to_date('31/03/2020','dd/mm/yyyy'))
# and d.PHA_ATC_C07 LIKE 'N02%'
# and a.DPN_QLF != '71'
# and a.BSE_PRS_NAT != '0'
# GROUP BY d.PHA_ATC_C07, a.BEN_SEX_COD
# );
# disconnect from oracle;
# quit;
# ```
#
# #### Pénicilines sur l'année 2018
#
# ```
# proc sql;
# %connectora;
# create table liste_patients3 as select * from connection to oracle
# (select
# 	COUNT(DISTINCT(a.BEN_NIR_PSA)) AS nb_patients,
# 	d.PHA_ATC_C07 AS ATC,
# 	a.BEN_SEX_COD AS sexe
# from ER_PRS_F a,
# ER_PHA_F b,
# IR_PHA_R d
# where a.FLX_DIS_DTD = b.FLX_DIS_DTD
# and a.FLX_TRT_DTD = b.FLX_TRT_DTD
# and a.FLX_EMT_TYP = b.FLX_EMT_TYP
# and a.FLX_EMT_NUM = b.FLX_EMT_NUM
# and a.FLX_EMT_ORD = b.FLX_EMT_ORD
# and a.ORG_CLE_NUM = b.ORG_CLE_NUM
# and a.DCT_ORD_NUM = b.DCT_ORD_NUM
# and a.PRS_ORD_NUM = b.PRS_ORD_NUM
# and a.REM_TYP_AFF = b.REM_TYP_AFF
# and trim(d.PHA_CIP_C13) = trim(b.PHA_PRS_C13)
# and (a.EXE_SOI_DTD between to_date('01/01/2018','dd/mm/yyyy') and to_date('31/12/2018','dd/mm/yyyy'))
# and (a.FLX_DIS_DTD between to_date('01/01/2018','dd/mm/yyyy') and to_date('31/03/2019','dd/mm/yyyy'))
# and d.PHA_ATC_C07 LIKE 'J01C%'
# and a.DPN_QLF != '71'
# and a.BSE_PRS_NAT != '0'
# GROUP BY d.PHA_ATC_C07, a.BEN_SEX_COD
# );
# disconnect from oracle;
# quit;
#
# ```
#
# #### Dosage TSH sur année 2017
#
# ```
# SELECT
#     COUNT(DISTINCT(a.BEN_NIR_PSA)) AS nb_patients,
#     b.BIO_PRS_IDE as code,
#     a.BEN_SEX_COD as sexe
# FROM
#     er_prs_f a,
#     er_bio_f b,
#     ir_bio_r c
# WHERE
#     c.BIO_PRS_IDE = b.BIO_PRS_IDE
#     and a.FLX_DIS_DTD = b.FLX_DIS_DTD
#     and a.FLX_TRT_DTD = b.FLX_TRT_DTD
#     and a.FLX_EMT_TYP = b.FLX_EMT_TYP and a.FLX_EMT_NUM = b.FLX_EMT_NUM
#     and a.FLX_EMT_ORD = b.FLX_EMT_ORD and a.ORG_CLE_NUM = b.ORG_CLE_NUM
#     and a.DCT_ORD_NUM = b.DCT_ORD_NUM and a.PRS_ORD_NUM = b.PRS_ORD_NUM
#     and a.REM_TYP_AFF = b.REM_TYP_AFF
#     and b.BIO_PRS_IDE IN (1488,1208,1211,1210,1212)
#     and (
#        a.EXE_SOI_DTD between to_date('01/01/2017', 'dd/mm/yyyy')
#        and to_date('31/12/2017', 'dd/mm/yyyy')
#      )
#     and (
#        a.FLX_DIS_DTD between to_date('01/02/2017', 'dd/mm/yyyy')
#        and to_date('01/02/2018', 'dd/mm/yyyy')
#     )
# GROUP BY a.BEN_SEX_COD, b.BIO_PRS_IDE
# ```
#
# #### Actes en 2019
#
# ```
# PROC SQL;
# %connectora;
# CREATE TABLE requete_opensnds_openlpp as select * from connection to oracle (
# 	SELECT COUNT(DISTINCT(a.BEN_NIR_PSA)) AS nb_patients, b.TIP_PRS_IDE as code, a.BEN_SEX_COD as sexe
# 	FROM er_prs_f a, er_tip_f b
# 	WHERE a.FLX_DIS_DTD = b.FLX_DIS_DTD
# 		and a.FLX_TRT_DTD = b.FLX_TRT_DTD
# 		and a.FLX_EMT_TYP = b.FLX_EMT_TYP
# 		and a.FLX_EMT_NUM = b.FLX_EMT_NUM
# 		and a.FLX_EMT_ORD = b.FLX_EMT_ORD
# 		and a.ORG_CLE_NUM = b.ORG_CLE_NUM
# 		and a.DCT_ORD_NUM = b.DCT_ORD_NUM
# 		and a.PRS_ORD_NUM = b.PRS_ORD_NUM
# 		and a.REM_TYP_AFF = b.REM_TYP_AFF
# 		and b.TIP_PRS_IDE LIKE ('110%')
# 		and (
# 		   a.EXE_SOI_DTD between to_date('01/01/2017', 'dd/mm/yyyy')
# 		   and to_date('31/05/2017', 'dd/mm/yyyy')
# 		 )
# 		 and (
# 		   a.FLX_DIS_DTD between to_date('01/02/2017', 'dd/mm/yyyy')
# 		   and to_date('01/07/2017', 'dd/mm/yyyy')
# 		 )
# 		GROUP BY a.BEN_SEX_COD, b.TIP_PRS_IDE
# );
# disconnect from oracle;
# QUIT;
# ```
#
# #### Sommes remboursement et dénombrement analgésique
#
#
# ```
# proc sql;
# %connectora;
# create table liste_patients5 as select * from connection to oracle
# (select
# 	COUNT(*) AS denombrement,
# 	SUM(b.PHA_ACT_QSN * b.PHA_ACT_PRU * (a.RGO_REM_TAU/100)) as remboursement,
# 	d.PHA_ATC_C07 AS ATC,
# 	a.BEN_SEX_COD AS sexe
# from ER_PRS_F a,
# ER_PHA_F b,
# IR_PHA_R d
# where a.FLX_DIS_DTD = b.FLX_DIS_DTD
# and a.FLX_TRT_DTD = b.FLX_TRT_DTD
# and a.FLX_EMT_TYP = b.FLX_EMT_TYP
# and a.FLX_EMT_NUM = b.FLX_EMT_NUM
# and a.FLX_EMT_ORD = b.FLX_EMT_ORD
# and a.ORG_CLE_NUM = b.ORG_CLE_NUM
# and a.DCT_ORD_NUM = b.DCT_ORD_NUM
# and a.PRS_ORD_NUM = b.PRS_ORD_NUM
# and a.REM_TYP_AFF = b.REM_TYP_AFF
# and trim(d.PHA_CIP_C13) = trim(b.PHA_PRS_C13)
# and (a.EXE_SOI_DTD between to_date('01/01/2018','dd/mm/yyyy') and to_date('31/12/2018','dd/mm/yyyy'))
# and (a.FLX_DIS_DTD between to_date('01/02/2018','dd/mm/yyyy') and to_date('01/02/2019','dd/mm/yyyy'))
# and d.PHA_ATC_C07 LIKE 'N02%'
# and a.DPN_QLF != '71'
# and a.BSE_PRS_NAT != '0'
# and b.PHA_DEC_TOP != 'D'
# GROUP BY d.PHA_ATC_C07, a.BEN_SEX_COD
# );
# disconnect from oracle;
# quit;
# ```
#
# #### Codéine par codes CIP sur l'année 2019
#
# ```
# proc sql;
# %connectora;
# create table liste_patients3 as select * from connection to oracle
# (select
# 	COUNT(DISTINCT(a.BEN_NIR_PSA)) AS nb_patients,
# 	d.PHA_CIP_C13 AS CIP,
# 	a.BEN_SEX_COD AS sexe
# from ER_PRS_F a,
# ER_PHA_F b,
# IR_PHA_R d
# where a.FLX_DIS_DTD = b.FLX_DIS_DTD
# and a.FLX_TRT_DTD = b.FLX_TRT_DTD
# and a.FLX_EMT_TYP = b.FLX_EMT_TYP
# and a.FLX_EMT_NUM = b.FLX_EMT_NUM
# and a.FLX_EMT_ORD = b.FLX_EMT_ORD
# and a.ORG_CLE_NUM = b.ORG_CLE_NUM
# and a.DCT_ORD_NUM = b.DCT_ORD_NUM
# and a.PRS_ORD_NUM = b.PRS_ORD_NUM
# and a.REM_TYP_AFF = b.REM_TYP_AFF
# and trim(d.PHA_CIP_C13) = trim(b.PHA_PRS_C13)
# and (a.EXE_SOI_DTD between to_date('01/01/2019','dd/mm/yyyy') and to_date('31/12/2019','dd/mm/yyyy'))
# and (a.FLX_DIS_DTD between to_date('01/02/2019','dd/mm/yyyy') and to_date('01/03/2020','dd/mm/yyyy'))
# and d.PHA_ATC_L07 LIKE '%CODEINE%'
# and a.DPN_QLF != '71'
# and a.BSE_PRS_NAT != '0'
# GROUP BY d.PHA_CIP_C13, a.BEN_SEX_COD
# );
# disconnect from oracle;
# quit;
#
# ```
#

# %%
import pyarrow.parquet as pq
import numpy as np
import pandas as pd
import pyarrow as pa
from pandasql import sqldf

# %%
df_openmedic = pd.read_parquet("/home/jovyan/work/data/opensnds/atc5")
df_openmedic

# %%
df_snds = pd.read_table("/home/jovyan/work/data/snds/N02_2019_SEXE.csv", sep=",")
df_snds

# %%
##Filtrage des données openmedic
mysql = lambda q: sqldf(q, globals())
res = mysql(
    "SELECT ATC5, SEXE, SUM(NBC) as nb_conso FROM df_openmedic WHERE year==2019 GROUP BY ATC5, SEXE"
)
res

# %%
fusion = mysql(
    "SELECT res.ATC5, res.SEXE, res.nb_conso AS conso_open, df_snds.NB_CONSOMMANTS AS conso_snds FROM res, df_snds WHERE ATC5=CODE AND df_snds.SEXE=res.SEXE"
)
fusion["diff"] = fusion["conso_open"] - fusion["conso_snds"]
fusion["prop_diff"] = fusion["diff"] / fusion["conso_snds"]
fusion.sort_values(by=["prop_diff"])

# %%
import statistics

print("Différence moyenne  :", statistics.mean(fusion["prop_diff"]))
print("Différence mediane  :", statistics.median(fusion["prop_diff"]))
print("Différence max  :", max(statistics.multimode(fusion["prop_diff"])))
print("Différence min  :", min(statistics.multimode(fusion["prop_diff"])))

# %%
import matplotlib.pyplot as plt
import numpy as np

hommes = fusion.query("SEXE == 1")
labels = hommes["ATC5"].tolist()

x = np.arange(len(labels))  # the label locations
width = 0.1  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width / 2, hommes["conso_open"].tolist(), width, label="")
rects2 = ax.bar(x + width / 2, hommes["conso_snds"].tolist(), width, label="")

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.FontSize = 40
ax.set_ylabel("Nb consommants")
ax.set_title("Nb consommants par code ATC")
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)

fig.tight_layout()

plt.show()

# %%
import statistics

print("Nombre de codes ATC : ", len(labels))
print("Différence moyenne  :", statistics.mean(hommes["prop_diff"]))
print("Différence mediane  :", statistics.median(hommes["prop_diff"]))
print("Standard Deviation of sample is : ", statistics.stdev(hommes["prop_diff"]))

# %%
import matplotlib.pyplot as plt
import numpy as np

femmes = fusion.query("SEXE == 2")
labels2 = femmes["ATC5"].tolist()

x2 = np.arange(len(labels2))  # the label locations
width = 0.1  # the width of the bars

fig, ax = plt.subplots()
rects12 = ax.bar(x2 - width / 2, femmes["conso_open"].tolist(), width, label="")
rects22 = ax.bar(x2 + width / 2, femmes["conso_snds"].tolist(), width, label="")

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.FontSize = 40
ax.set_ylabel("Nb consommants")
ax.set_title("Nb consommants par code ATC")
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

ax.bar_label(rects12, padding=3)
ax.bar_label(rects22, padding=3)

fig.tight_layout()

plt.show()

# %%
print("Nombre de codes ATC : ", len(labels2))
print("Différence moyenne  :", statistics.mean(femmes["prop_diff"]))
print("Différence mediane  :", statistics.median(femmes["prop_diff"]))
print("Standard Deviation of sample is : ", statistics.stdev(femmes["prop_diff"]))

# %% [markdown]
# ### Péniciline sur l'année 2018
#

# %%
df_snds2 = pd.read_table("/home/jovyan/work/data/snds/J01C_2018_SEXE.csv", sep=",")
df_snds2

# %%
res2 = mysql(
    "SELECT ATC5, SEXE, SUM(NBC) as nb_conso FROM df_openmedic WHERE year==2018 GROUP BY ATC5, SEXE"
)
res2

# %%
fusion2 = mysql(
    "SELECT res2.ATC5, res2.SEXE, res2.nb_conso AS conso_open, df_snds2.NB_CONSOMMANTS AS conso_snds FROM res2, df_snds2 WHERE ATC5=CODE AND df_snds2.SEXE=res2.SEXE"
)
fusion2["diff"] = fusion2["conso_open"] - fusion2["conso_snds"]
fusion2["prop_diff"] = fusion2["diff"] / fusion2["conso_snds"]
fusion2.sort_values(by=["prop_diff"])

# %%
fusion2["prop_diff"].describe()

# %%
print("Différence moyenne  :", statistics.mean(fusion2["prop_diff"]))
print("Différence mediane  :", statistics.median(fusion2["prop_diff"]))
print("Différence max  :", max(statistics.multimode(fusion2["prop_diff"])))
print("Différence min  :", min(statistics.multimode(fusion2["prop_diff"])))

# %%
hommes2 = fusion2.query("SEXE == 1")
labels = hommes2["ATC5"].tolist()

x = np.arange(len(labels))  # the label locations
width = 0.1  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width / 2, hommes2["conso_open"].tolist(), width, label="")
rects2 = ax.bar(x + width / 2, hommes2["conso_snds"].tolist(), width, label="")

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.FontSize = 40
ax.set_ylabel("Nb consommants")
ax.set_title("Nb consommants par code ATC")
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)

fig.tight_layout()

plt.show()

# %% [markdown]
# ### TSH sur l'année 2017

# %%
df_openbio = pd.read_parquet("/home/jovyan/work/data/openbio/acte")
df_openbio

# %%
df_snds3 = pd.read_table("/home/jovyan/work/data/snds/TSH_2017_SEXE.csv", sep=";")
df_snds3

# %%
res3 = mysql(
    "SELECT ACTE, SEXE, SUM(NBC) as nb_conso FROM df_openbio WHERE year==2017 GROUP BY ACTE, SEXE"
)
res3

# %%
fusion3 = mysql(
    "SELECT res3.ACTE, res3.SEXE, res3.nb_conso AS conso_open, df_snds3.NB_CONSOMMANTS AS conso_snds FROM res3, df_snds3 WHERE ACTE=CODE AND df_snds3.SEXE=res3.SEXE"
)
fusion3["diff"] = fusion3["conso_open"] - fusion3["conso_snds"]
fusion3["prop_diff"] = fusion3["diff"] / fusion3["conso_snds"]
fusion3.sort_values(by=["prop_diff"])

# %%
fusion3["prop_diff"].describe()

# %%
df_openlpp = pd.read_parquet("/home/jovyan/work/data/openlpp/code_lpp")
df_openlpp

# %%
df_snds4 = pd.read_table("/home/jovyan/work/data/snds/LPP_110_2017_SEXE.csv", sep=";")
df_snds4

# %%
res4 = mysql(
    "SELECT CODE_LPP, SEXE, SUM(NBC) as nb_conso FROM df_openlpp WHERE year==2017 GROUP BY CODE_LPP, SEXE"
)
res4

# %%
fusion4 = mysql(
    "SELECT res4.CODE_LPP, res4.SEXE, res4.nb_conso AS conso_open, df_snds4.NB_PATIENTS AS conso_snds FROM res4, df_snds4 WHERE CODE_LPP=CODE AND df_snds4.SEXE=res4.SEXE"
)
fusion4["diff"] = fusion4["conso_open"] - fusion4["conso_snds"]
fusion4["prop_diff"] = fusion4["diff"] / fusion4["conso_snds"]
fusion4.sort_values(by=["prop_diff"])

# %%
fusion4["prop_diff"].describe()

# %% [markdown]
# ### Dénombrement et remboursements des analgésiques sur l'année 2018

# %%
df_snds5 = pd.read_table(
    "/home/jovyan/work/data/snds/N02_SOMME_2018_ajuste.csv", sep=","
)
df_snds5

# %%
##Filtrage des données openmedic
mysql = lambda q: sqldf(q, globals())
res5 = mysql(
    "SELECT SUM(BOITES) AS SUM_BOITES, SUM(REM) AS SUM_REM, ATC5, SEXE FROM df_openmedic WHERE year==2018 AND ATC5 LIKE 'N02%' GROUP BY ATC5, SEXE"
)
res5

# %%
fusion5 = mysql(
    "SELECT res5.ATC5, res5.SEXE, res5.SUM_REM AS rem_open, res5.SUM_BOITES as den_open, df_snds5.Somme_Remb AS rem_snds, df_snds5.Denombrement AS den_snds FROM res5, df_snds5 WHERE ATC5=CODE AND df_snds5.SEXE=res5.SEXE"
)
fusion5["diff_rem"] = fusion5["rem_open"] - fusion5["rem_snds"]
fusion5["prop_diff_rem"] = 100 * (fusion5["diff_rem"] / fusion5["rem_snds"])
fusion5["diff_den"] = fusion5["den_open"] - fusion5["den_snds"]
fusion5["prop_diff_den"] = 100 * (fusion5["diff_den"] / fusion5["den_snds"])
pd.set_option("display.precision", 3)
pd.options.display.float_format = "{:,.2f}".format
display(fusion5.sort_values(by=["prop_diff_rem"]))

# %%
labels = fusion5["ATC5"].tolist()
print("Nombre de codes ATC : ", len(labels) / 2)
print("Différence moyenne  :", statistics.mean(fusion5["prop_diff_rem"]))
print("Différence mediane  :", statistics.median(fusion5["prop_diff_rem"]))
print("Standard Deviation of sample is : ", statistics.stdev(fusion5["prop_diff_rem"]))

# %%
fusion5["prop_diff_rem"].describe()

# %%
fusion5["prop_diff_den"].describe()

# %%
df_snds6 = pd.read_table("/home/jovyan/work/data/snds/CIP_COD_2018_SEXE.csv", sep=",")
df_snds6

# %%
df_openmedic_cip = pd.read_parquet("/home/jovyan/work/data/opensnds/cip13")
df_openmedic_cip

# %%
mysql = lambda q: sqldf(q, globals())
res6 = mysql(
    "SELECT SUM(NBC) AS NB_CONSO, CIP13, SEXE FROM df_openmedic_cip WHERE year==2019 GROUP BY CIP13, SEXE"
)
res6

# %%
fusion6 = mysql(
    "SELECT res6.CIP13, res6.SEXE, res6.NB_CONSO AS conso_open, df_snds6.NB_CONSO AS conso_snds FROM res6, df_snds6 WHERE CIP13=CODE AND df_snds6.SEXE=res6.SEXE"
)
fusion6["diff"] = fusion6["conso_open"] - fusion6["conso_snds"]
fusion6["prop_diff"] = fusion6["diff"] / fusion6["conso_snds"]
fusion6.sort_values(by=["prop_diff"])

# %%
print("Différence moyenne  :", statistics.mean(fusion6["prop_diff"]))
print("Différence mediane  :", statistics.median(fusion6["prop_diff"]))
print("Différence max  :", max(statistics.multimode(fusion6["prop_diff"])))
