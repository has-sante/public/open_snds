## Commandes pour générer la documentation

Se placer dans le virtuel env dans le dossier docs
`cd ./docs && poetry shell`

Si le dossier de documentation est vide, initialiser le projet
`sphinx-quickstart`

Générer la documentation à partir du code du projet
`sphinx-apidoc -o source/ ../opensnds/`

Générer le HTML
`make html`