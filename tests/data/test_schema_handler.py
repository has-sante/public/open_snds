from os import getsid
from opensnds.preprocessing.schema_handler import get_nomenclature, load_schemas
import unittest


# load schemas
schemas = load_schemas()


class TestSchemaHandler(unittest.TestCase):
    def test_get_nomenclature(self):
        opendamir_schema = schemas["opendamir"]
        nom, primary_key = get_nomenclature("PRS_NAT", opendamir_schema)
        nom.sort_values("PRS_NAT", inplace=True)
        self.assertEqual(nom.iloc[0]["PRS_NAT"], 0)
        self.assertEqual(primary_key, ["PRS_NAT"])


if __name__ == "__main__":
    unittest.main()
