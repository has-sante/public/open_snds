from opensnds.constants import DNB, REM
import unittest
from opensnds.app.utils.back_utils import load_datasets, query_table
from opensnds.preprocessing.schema_handler import load_schemas

DEBUG = False
datasets = load_datasets(DEBUG)
schemas = load_schemas()
ds_name = "openbio"


class TestSchemaHandler(unittest.TestCase):
    def test_get_nomenclature(self):
        test_payload = {
            "ds_name": ds_name,
            "dataset": datasets[ds_name],
            "group_cols": "sex",
            "filter_col": "ACTE",
            "filter_values": [1608],  # potassium
            "years": 2018,
            "debug": DEBUG,
        }
        # verification by hand
        #  true_df = datasets["openbio"].query(
        #      "year in (2018) and ACTE in ({})".format(
        #          ", ".join(map(str, test_payload["filter_values"])))
        #      ).groupby("SEXE").agg(
        #     **{NB_ACTES: ks.NamedAgg("DNB", "sum"),
        #        REM: ks.NamedAgg("REM", "sum")}
        # )

        df = query_table(**test_payload)
        self.assertEqual(df.loc[df["SEX_LIB"] == "FEMININ", DNB] == 1308459)
        self.assertEqual(df.loc[df["SEX_LIB"] == "FEMININ", REM] == 95244.70)


if __name__ == "__main__":
    unittest.main()
