
build:
	podman build -t 'opensnds:latest' .

build-sphinx:
	podman build -t 'opensnds-docs:latest' . --build-arg DOCS_DEPENDENCIES=true

compile:
	docker-compose -f docker-compose.compile.yml up -d

up-prod:
	docker-compose -f docker-compose.yml up -d

down-prod:
	docker-compose down

logs-prod:
	docker-compose -f docker-compose.yml logs -f

logs-compile:
	docker-compose -f docker-compose.compile.yml logs -f

down:
	docker-compose down

run-dev-container :
	podman run --rm -it --env-file $(shell pwd)/.env_dev -p 8050:8050 -v $(shell pwd)/:/opt/open_snds opensnds:latest /bin/bash

run-dev-container-sphinx :
	podman run --rm -it -v $(shell pwd)/:/opt/open_snds opensnds-docs:latest /bin/bash

