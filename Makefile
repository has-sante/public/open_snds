
build:
	podman build -t 'opensnds:latest' .

build-sphinx:
	podman build -t 'opensnds-docs:latest' . --build-arg DOCS_DEPENDENCIES=true

compile:
	podman-compose -f docker-compose.compile.yml up -d

up-prod:
	podman-compose -f docker-compose.yml up -d

down-prod:
	podman-compose down

logs-prod:
	podman-compose -f docker-compose.yml logs -f

logs-compile:
	podman-compose -f docker-compose.compile.yml logs -f

down:
	podman-compose down

run-dev-container :
	podman run --rm -it --env-file $(shell pwd)/.env_dev -p 8050:8050 -v $(shell pwd)/:/opt/open_snds opensnds:latest /bin/bash

run-dev-container-sphinx :
	podman run --rm -it -v $(shell pwd)/:/opt/open_snds opensnds-docs:latest /bin/bash

